// [SECTION] Dependencies
const jwt = require("jsonwebtoken");
const secret = "EcommerceAPI";

// [SECTION] Functions
// Create token
module.exports.createToken = (result) => {
	const data = {
		id: result._id,
		email: result.email,
		isAdmin: result.isAdmin
	};

	return jwt.sign(data, secret, {});
}

// Token verification
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;

	if (token) {
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, err => {
			if(err){
				return res.send('Invalid token');
			} else {
				next();
			}
		})
	} else {
		return res.send('No token provided.');
	}
}

// Decode a token
module.exports.decode = (token) => {
	if(!token) {
		return null;
	} else {
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, err => {
			if(err){
				return null;
			} else{
				return jwt.decode(token, {complete: true}).payload
			}
		})
	}
}
