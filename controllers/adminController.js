// [SECTION] Dependencies
const Admin = require("../models/Admin");
const bcrypt = require("bcrypt");
const auth	= require("../auth");
const Client = require("../models/Client");

// [SECTION] Functions
// Admin registration and availability
module.exports.register = (reqBody, res) => {
	let newAdmin = new Admin ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	})

	return Admin.find({email: reqBody.email})
		.then(result => {
			if(result.length > 0) {
				return "Email already in use";
			} else {
				return newAdmin.save()
					.then(client => {
						console.log(newAdmin);
						return "Registration successful";
					})
					.catch(err => {
						return "Error";
					})
			}
		})
}

// Admin login
module.exports.login = (reqBody) => {
	return Admin.findOne({email: reqBody.email})
		.then(result => {
			if(!result){
				return "Register first";
			} else {
				const checkPassword = bcrypt.compareSync(reqBody.password, result.password);

				if(checkPassword){
					return {accessToken: auth.createToken(result)};
				} else {
					return "Error";
				}
			}
		})
}

// Retrieving admin details
module.exports.getProfile = (data) => {
	return Admin.findById(data.adminId)
		.then(result => {
			result.password = "";

			const reorderedProfile = {
				_id: result._id,
				firstName : result.firstName,
				lastName: result.lastName,
				email: result.email,
				password: result.password,
				mobileNo: result.mobileNo,
				acctsManaged: result.acctsManaged,
				isActive: result.isActive,
				createdOn: result.createdOn
			}
		return reorderedProfile;
		})
}

// Assign admin(acct manager) to a client(acct)
module.exports.assign = (data) => {
  	return Admin.findById(data.adminId)
    	.then(admin => {
      		if (!admin) {
    		    return 'Admin not found';
      		}

      		return Client.findById(data.clientId)
        		.then(client => {
          			if (!client) {
            			return 'Client not found';
          			}

          			// Check if the client is already assigned to the admin
          			if (admin.acctsManaged.some(acct => acct.clientId.toString() === data.clientId)) {
            			return 'Client is already assigned to this admin';
          			}

          			// Get the firstName and lastName of the account manager
          			return Admin.findById(data.adminId, 'firstName lastName')
            			.then(accountManager => {
              				let newAcctManaged = {
                				clientId: data.clientId,
                				acctName: client.acctName,
                				transactions: client.transactions
              				};

              				admin.acctsManaged.push(newAcctManaged);

              				return admin.save()
                				.then(isAdminUpdated => {
                  					client.acctMgr.push({
                    					adminId: data.adminId,
                    					firstName: accountManager.firstName,
                    					lastName: accountManager.lastName
                 					});

                  					return client.save()
                    					.then(isClientUpdated => {
                      						if (isAdminUpdated && isClientUpdated) {
                        							return 'Client successfully assigned';
                      						} else {
                        						return 'Failed to assign admin to client';
                      						}
                    					});
                				});
            			});
        		});
    	})
    	.catch(error => {
      		return 'Error during assignment';
    	});
};

// Retrieve all clients/ accounts
module.exports.getAllClients = () => {
	return Client.find({})
		.then((results) => {
			const reorderedAccounts = results.map((result) => {
				return {
					_id: result._id,
					acctName: result.acctName,
					email: result.email,
					address: result.address,
					createdOn: result.createdOn,
					acctRep: result.acctRep,
					mobileNo: result.mobileNo,
					isActive: result.isActive,
					acctMgr: result.acctMgr,
					carts: result.carts,
					cart: result.card
				}
			})
			return reorderedAccounts;
		})
}

// Retrieve all clients/ accounts of a specific account manager
module.exports.getMyClients = (data) => {
  return Client.find({ 'acctMgr.adminId': data.adminId })
    .then((results) => {
      const reorderedAccounts = results.map((result) => {
        return {
          _id: result._id,
          acctName: result.acctName,
          email: result.email,
          address: result.address,
          createdOn: result.createdOn,
          acctRep: result.acctRep,
          mobileNo: result.mobileNo,
          isActive: result.isActive,
          acctMgr: result.acctMgr,
          carts: result.carts,
          cart: result.cart
        };
      });
      return reorderedAccounts;
      return result;
    });
};


// Modify an admin profile's details
module.exports.updateProfile = (reqParams, updateData) => {
	const adminId = reqParams.adminId;

	return Admin.findByIdAndUpdate(adminId, {$set: updateData}, {new: true})
		.then((result) => {
				result.password = "";

				const reorderedProfile = {
					_id: result._id,
					firstName : result.firstName,
					lastName: result.lastName,
					email: result.email,
					password: result.password,
					mobileNo: result.mobileNo,
					acctsManaged: result.acctsManaged,
					isActive: result.isActive,
					createdOn: result.createdOn
				}
			return reorderedProfile;
			})
}

// Archive a client
module.exports.archiveClient = (reqParams) => {
	let updateActiveField = {
		isActive: false
	}

	return Client.findByIdAndUpdate(reqParams.clientId, updateActiveField)
		.then((product, error) => {
			if(error){
				return "Error";
			} else {
				return "Client archived.";
			}
		})
}
