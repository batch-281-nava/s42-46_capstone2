// [SECTION] Declaration
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');


// [SECTION] Functions
// Adding a product - checks if SKU is already in use
module.exports.addProduct = (data) => {
  	// Check if SKU is already in use
  	return Product.exists({ sku: data.product.sku })
    	.then((exists) => {
      		if (exists) {
        		// SKU is already in use
        		return "SKU already in use";
      		} else {
        		// Create a new product
        		let newProduct = new Product({
          			sku: data.product.sku,
          			businessUnit: data.product.businessUnit,
          			category: data.product.category,
          			description: data.product.description,
          			origPrice: data.product.origPrice,
        		});

        	const vatRate = 0.03;
        	newProduct.percentVat = newProduct.origPrice * vatRate;
        	newProduct.srp = newProduct.origPrice + newProduct.percentVat;

        	return newProduct
          		.save()
          		.then((product, error) => {
            		if (error) {
              			return "Unsuccessful.";
            		} else {
              			return "Product is uploaded";
            		}
          		});
      		}
    	})
    	.catch((error) => {
      		console.error(error);
      		return false;
    	});
};

// Adding multiple products
module.exports.addProducts = (data) => {
  	const products = data.products.map((productData) => {
    	return Product.exists({ sku: productData.sku })
    		.then((exists) => {
	      		if (exists) {
	        		// SKU is already in use
	        		return "SKU already in use";
	      		} else {
	        		let newProduct = new Product({
			          		sku: productData.sku,
			          		businessUnit: productData.businessUnit,
			          		category: productData.category,
			          		description: productData.description,
			          		origPrice: productData.origPrice,
	        		});

	        	const vatRate = 0.03;
	        		newProduct.percentVat = newProduct.origPrice * vatRate;
	        		newProduct.srp = newProduct.origPrice + newProduct.percentVat;

	        	return newProduct;
	      		}
    		});
  	});

  	return Promise.all(products)
    	.then((newProducts) => {
      		// Filter out products that have 'false' value (already in use)
      		const validProducts = newProducts.filter((product) => product !== false);

      		// Insert the valid products into the database
      		return Product.insertMany(validProducts)
        		.then(() => true)
        		.catch((error) => {
          			console.error(error);
          			return false;
        	});
   		})
    	.catch((error) => {
      		console.error(error);
      		return false;
    	});
};

// Retrieve all products
module.exports.getAllProducts = () => {
  	return Product.find({})
    	.then((results) => {
      		const reorderedProducts = results.map((result) => {
        		return {
          			_id: result._id,
          			sku: result.sku,
          			businessUnit: result.businessUnit,
          			category: result.category,
          			description: result.description,
          			isActive: result.isActive,
          			onSale: result.onSale,
          			srp: result.srp,
          			inventory: result.inventory
        		};
      		});

      		return reorderedProducts;
    	});
};

// Retrieve all ACTIVE products
module.exports.getAllActive = () => {
  	return Product.find({isActive: true})
    	.then((results) => {
      		const reorderedProducts = results.map((result) => {
        		return {
          			_id: result._id,
          			sku: result.sku,
          			businessUnit: result.businessUnit,
          			category: result.category,
          			description: result.description,
          			isActive: result.isActive,
          			onSale: result.onSale,
          			srp: result.srp,
          			inventory: result.inventory
        		};
      		});

      		return reorderedProducts;
    	});
};

// Retrieve ON SALE products
module.exports.getOnSale = () => {
  	return Product.find({onSale: true})
    	.then((results) => {
      		const reorderedSale = results.map((result) => {
        		return {
          			_id: result._id,
          			sku: result.sku,
          			businessUnit: result.businessUnit,
          			category: result.category,
          			description: result.description,
          			isActive: result.isActive,
          			onSale: result.onSale,
          			srp: result.srp,
          			inventory: result.inventory
        		};
      		});
      		return reorderedSale;
    	});
};

// Retrieve products less than the client's budget
module.exports.getOnBudget = (maxPrice) => {
  	const query = maxPrice ? { srp: { $lt: maxPrice } } : {};

  	return Product.find(query)
    	.then((results) => {
      		const reorderedProducts = results.map((result) => {
        		return {
          			_id: result._id,
          			sku: result.sku,
          			businessUnit: result.businessUnit,
          			category: result.category,
          			description: result.description,
          			isActive: result.isActive,
          			onSale: result.onSale,
          			srp: result.srp,
          			inventory: result.inventory
        		};
      		});

      	return reorderedProducts;
    });
};

// Retrieve a single product based on product Id
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId)
    	.then((result) => {
      		if (result) {
        		const reorderedProduct = {
          			_id: result._id,
          			sku: result.sku,
          			businessUnit: result.businessUnit,
          			category: result.category,
          			description: result.description,
          			isActive: result.isActive,
          			onSale: result.onSale,
          			srp: result.srp,
          			inventory: result.inventory
        		};
        	return reorderedProduct;
      		} else {
        	return null; // Return null if product is not found
      	}
    	});
};

// Updating a product detail
module.exports.updateProduct = (reqParams, updateData) => {
  	const productId = reqParams.productId;

  	if(updateData.origPrice) {
  		const vatRate = 0.03;
  			updateData.percentVat = updateData.origPrice * vatRate;
  			updateData.srp = updateData.origPrice + updateData.percentVat;
  	}

  	return Product.findByIdAndUpdate(productId, { $set: updateData }, { new: true })
    	.then((result) => {
      		if (result) {
        		const reorderedProduct = {
          			_id: result._id,
          			sku: result.sku,
          			businessUnit: result.businessUnit,
          			category: result.category,
          			description: result.description,
          			isActive: result.isActive,
          			onSale: result.onSale,
          			srp: result.srp,
          			inventory: result.inventory
        		};
        	
        	return reorderedProduct;
      		
      		} else {
        		return null; // Return null if product is not found
      		}
    	});
};

// Archive a product
module.exports.archiveProduct = (reqParams) => {
	let updateActiveField = {
		isActive: false
	}

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField)
		.then((product, error) => {
			if(error){
				return "Unsuccessful";
			} else {
				return "Product archived";
			}
		});
}
