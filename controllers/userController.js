// [SECTION] Dependencies
const Client = require("../models/Client");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// [SECTION] Functions
// Client registration and availability
module.exports.register = (reqBody) => {
	let newClient = new Client ({
		acctName: reqBody.acctName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		address: {
			houseNo: reqBody.address.houseNo,
			street: reqBody.address.street,
			barangay: reqBody.address.barangay,
			city: reqBody.address.city,
			province: reqBody.address.province,
			country: reqBody.address.country
		},
		mobileNo: reqBody.mobileNo,
		acctRep: reqBody.acctRep
	})

	return Client.find({email: reqBody.email})
		.then(result => {
			if(result.length > 0) {
				return "Email address is already registered";
			} else {
				return newClient.save()
					.then(client => {
						console.log(newClient);
						return "Registration successful";
					})
					.catch(err => {
						return "Registration unsuccessful";
					})
			}
		})
}

// User authentication/ login
module.exports.login = (reqBody) => {
	return Client.findOne({email: reqBody.email})
		.then(result => {
			if(!result){
				return "The email you entered isn’t connected to an account.";
			} else {
				const checkPassword = bcrypt.compareSync(reqBody.password, result.password);

				if(checkPassword){
					// return "Login successful.";
					return {accessToken: auth.createToken(result)};
				} else {
					return "Error";
				}
			}
		})
}

// Retrieving client details
module.exports.getProfile = (data) => {
	return Client.findById(data.clientId)
		.then(result => {
		result.password = "";

		// Reordering the necessary fields since address contains subdocuments
		const reorderedProfile = {
			_id: result._id,
			acctName: result.acctName,
			email: result.email,
			password: result.password,
			createdOn: result.createdOn,
			address: result.address,
			acctRep: result.acctRep,
			mobileNo: result.mobileNo,
			acctMgr: result.acctMgr,
			carts: result.carts,
			isActive: result.isActive,
		}

		return reorderedProfile;
	});
}

// Modify a profile detail
module.exports.updateProfile = (reqParams, updateData) => {
	const clientId = reqParams.clientId;

	return Client.findByIdAndUpdate(clientId, {$set: updateData}, {new: true})
		.then((result) => {
			if(result) {
				result.password = "";

				const reorderedProfile = {
					_id: result._id,
					acctName: result.acctName,
					email: result.email,
					password: result.password,
					createdOn: result.createdOn,
					address: result.address,
					acctRep: result.acctRep,
					mobileNo: result.mobileNo,
					acctMgr: result.acctMgr,
					carts: result.carts,
					isActive: result.isActive,
				}

				return reorderedProfile;
			} else {
				return "Update unsuccessful.";
			}
		})
}
