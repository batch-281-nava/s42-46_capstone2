// [SECTION] Declarations
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// [SECTION] Imports 
// const clientRoutes = require('./routes/clientRoutes');
const productRoutes = require('./routes/productRoutes');
const adminRoutes = require('./routes/adminRoutes');
const orderRoutes = require('./routes/orderRoutes');

const app = express();

// [SECTION] Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// [SECTION] Defines the string to be included for all user routes defined in the routes file
// app.use('/clients', clientRoutes);
app.use('/products', productRoutes);
app.use('/admin', adminRoutes);
app.use('/order', orderRoutes);

// [SECTION] DB connection
mongoose.set('strictQuery', true);
mongoose.connect("mongodb+srv://marjorienava111:test123@cluster0.aoqhanq.mongodb.net/capstone?retryWrites=true&w=majority", {
	// Deprecators: It will give warnings if there are concerns
	useNewUrlParser : true,
	useUnifiedTopology : true
});

let db = mongoose.connection;
db.on('error', console.error.bind(console, "MongoDB Connection Error."));
db.once('open', () => console.log('Now connected to MongoDB Atlas!'));

// [SECTION] Listen
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT|| 4000}` )
})

