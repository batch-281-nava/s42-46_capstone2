// [SECTION] Dependencies
const mongoose = require('mongoose');

// [SECTION] Schema declaration
const adminSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required."]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required."]
	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	acctsManaged: [{
		clientId: {
			type: String
		},
		acctName: {
			type: String
		}
	}],
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
})

// [SECTION] Export
module.exports = mongoose.model("Admin", adminSchema);
