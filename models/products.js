// [SECTION] Dependencies
const mongoose = require('mongoose');

// [SECTION] Schema declaration
const productSchema = new mongoose.Schema({
	sku : {
		type: String,
		required: [true, "SKU is required."]
	},
	productItems: {
		type: String,
		enum: ['T-shirt', 'Mugs', 'Towel'],
		required: [true, "product Items is required."]
	},
	categoryDesign: {
	  	type: String,
	  	required: [true, "Category Design is required."],
	  	enum: ['Personalised photo', 'Anime', 'Cartoon', 'Animals', 'Minimalist', 'Plain']
	},
	description: {
		type: String,
		required: [true, "Description is required."]
	},
	origPrice: {
		type: Number,
	},
	percentVat: {
		type: Number
	},
	srp: {
		type: Number,
	},
	inventory: {
		type: Number,
		default: 0
	},
	isActive: {
		type: Boolean,
		default: true
	},
	onSale: {
		type: Boolean,
		default: false
	},
	createdOn: {
		type: Date,
		default: Date()
	}

});


// [SECTION] Export
module.exports = mongoose.model('Product', productSchema);
