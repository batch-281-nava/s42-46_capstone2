// [SECTION] Dependencies
const express = require('express');
const router = express.Router();
const adminController = require("../controllers/adminController");
const auth = require("../auth");

// [SECTION] Routes
// Route for admin registration
router.post("/register", (req, res) => {
	adminController.register(req.body)
		.then(resultFromController => res.send(resultFromController));
})

// Route for login/ authentication
router.post("/login", (req, res) => {
	adminController.login(req.body)
		.then(resultFromController => res.send(resultFromController));
})

// Route for retrieving account details
router.get("/details", (req, res) => {
	const adminData = auth.decode(req.headers.authorization);

	adminController.getProfile({adminId: adminData.id})
		.then(resultFromController => res.send(resultFromController));
})

// Route for assigning a client to an admin
router.post("/assign", auth.verify, (req, res) => {
	const data = {
		adminId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		clientId: req.body.clientId
	}

	if(data.isAdmin == true){
		adminController.assign(data)
			.then(resultFromController => res.send(resultFromController));
	} else{
		return false
	}
})

// Route for retrieving all accts/ clients
router.get("/accounts", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){
		adminController.getAllClients(data)
			.then(resultFromController => res.send(resultFromController));
	} else {
		return false
	}
})

// Route for retrieving accts under an admin
router.get("/myAccounts", auth.verify, (req, res) => {
	const data = {
		adminId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){
		adminController.getMyClients(data)
			.then(resultFromController => res.send(resultFromController));
	} else {
		return false
	}
})

// Route for updating admin profile
router.put("/:adminId", auth.verify, (req, res) => {
	const adminId = req.params.adminId;
	const updateData = req.body;

	adminController.updateProfile({adminId}, updateData)
		.then(resultFromController => res.send(resultFromController));
})

// Route for archiving client/ acct
router.put("/:clientId/archive", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){
		adminController.archiveClient(req.params)
			.then(resultFromController => res.send(resultFromController));
	} else {
		return false;
	}
})


// [SECTION] Export
module.exports = router;
