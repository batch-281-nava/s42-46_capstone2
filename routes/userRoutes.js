// [SECTION] Dependencies
const express = require('express');
const router = express.Router();
const clientController = require("../controllers/clientController");
const auth = require("../auth");

// [SECTION] Routes
// Route for user registration
router.post("/register", (req, res) => {
	clientController.register(req.body)
		.then(resultFromController => res.send(resultFromController));
})

// Route for login/ authentication
router.post("/login", (req, res) => {
	clientController.login(req.body)
		.then(resultFromController => res.send(resultFromController));
})

// Route for retrieving account details
router.post("/details", (req, res) => {
	const clientData = auth.decode(req.headers.authorization);

	clientController.getProfile({clientId: clientData.id})
		.then(resultFromController => res.send(resultFromController));
})

// Route for modifying a client profile
router.put("/:clientId", auth.verify, (req, res) => {
	const clientId = req.params.clientId;
	const updateData = req.body;

	clientController.updateProfile({clientId}, updateData)
		.then(resultFromController => res.send(resultFromController));
})


// [SECTION] Export
module.exports = router;
